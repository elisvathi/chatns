import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { AppRoutingModule } from "./app.routing";
import { AppComponent } from "./app.component";

import { HomeComponent } from "./components/home/home.component";
import { RoutesService } from "./services/routes.service";
import { RestService } from "./services/rest.service";
import { AuthService } from "./services/auth.service";

// Uncomment and add to NgModule imports if you need to use two-way binding
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import {NativeScriptRouterModule} from 'nativescript-angular/router'
// Uncomment and add to NgModule imports  if you need to use the HTTP wrapper
import { NativeScriptHttpModule } from "nativescript-angular/http";
import { LoginScreenComponent } from "./login-screen/login-screen.component";
import { LoginFormComponent } from "./login-form/login-form.component";
import { LandingComponent } from "./components/landing/landing.component";
import { FullConversationComponent } from "./components/full-conversation/full-conversation.component";
import { ConversationListComponent } from "./components/conversation-list/conversation-list.component";
import { UserProfileComponent } from "./components/user-profile/user-profile.component";
import { UsersListComponent } from "./components/users-list/users-list.component";
import { CrudService } from "./services/crud.service";
import { DataService } from "./services/data.service";
import { ConversationTitleComponent } from "./components/conversation-title/conversation-title.component";
import { UserRowComponent } from "./components/user-row/user-row.component";

import { SocketService } from "./services/socket.service";
import { SingleMessageComponent } from "./components/single-message/single-message.component";


import { NativeScriptUIListViewModule } from "nativescript-pro-ui/listview/angular";
import { DataMessageGroupComponent } from "./components/data-message-group/data-message-group.component";
import { MessageGroupComponent } from "./components/message-group/message-group.component";
import { DiscoverComponent } from "./components/discover/discover.component";
import * as platform from 'platform';
declare var GMSServices: any;
if(platform.isIOS){
    GMSServices.provideAPIKey("AIzaSyAd1uI2QSLK5_B7JNNjpGEFwgmZYSxU2_k");
}

@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        NativeScriptModule,
        AppRoutingModule,
        NativeScriptHttpModule,
        NativeScriptFormsModule,
        NativeScriptUIListViewModule
    ],
    declarations: [
        AppComponent,
        LoginFormComponent,
        LoginScreenComponent,
        HomeComponent,
        LandingComponent,
        FullConversationComponent,
        ConversationListComponent,
        ConversationTitleComponent,
        UserProfileComponent,
        UsersListComponent,
        UserRowComponent,
        SingleMessageComponent,
        DataMessageGroupComponent,
        MessageGroupComponent,
        DiscoverComponent,
    ],
    providers: [
       AuthService,
       RestService,
       RoutesService,
       DataService,
       CrudService,

       SocketService
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
/*
Pass your application module to the bootstrapModule function located in main.ts to start your app
*/
export class AppModule { }
