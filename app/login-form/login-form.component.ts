import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Component({
  moduleId: module.id,
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {
  isLoggingIn: boolean = true;
  username: string = "";
  password: string = "";
  confirmPassword: string = "";
  temptext="";
  constructor(private auth: AuthService) { }

  ngOnInit() { }
  swapForm(){
    this.isLoggingIn = !this.isLoggingIn;
  }
 tryLogIn(){
   if(this.username=="" || this.password == ""){this.auth.getToken('elisvathi@outlook.com', 'vathielis')}
   else{this.auth.getToken(this.username, this.password);}
    
  }

}
