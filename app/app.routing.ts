import { NgModule } from "@angular/core";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { Routes } from "@angular/router";
import { FullConversationComponent } from "./components/full-conversation/full-conversation.component";
import { LandingComponent } from "./components/landing/landing.component";
import { HomeComponent } from "./components/home/home.component";


const routes: Routes = [
    {path: 'conversation/:id', component: FullConversationComponent},
    {path: 'landing', component: LandingComponent},
    {path: 'home', component: HomeComponent}
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }