import { Injectable } from '@angular/core';
import { RoutesService } from './routes.service';
import { Http, Headers, Response} from "@angular/http";
import { Observable as RxObservable} from "rxjs/Observable";

import "rxjs/add/operator/map";
import "rxjs/add/operator/do";
import "rxjs/add/operator/toPromise"
import { AuthService } from './auth.service';

@Injectable()
export class RestService {

  constructor(private http: Http,private auth: AuthService) { }
  public get(url){
    return this.http.get(url,{headers: this.auth.headers}).toPromise();
  }
  public post(data, url):Promise<Response>{
    return this.http.post(url, data, {headers: this.auth.headers}).toPromise();
  }
  public update(data, url):Promise<Response>{
    return this.http.patch(url, data, {headers: this.auth.headers}).toPromise();
  }
  public delete(url):Promise<Response>{
    return this.http.delete(url, {headers: this.auth.headers}).toPromise();
  }
  

}
