import { Injectable } from '@angular/core';
import { RoutesService } from './routes.service';
import { Http, Headers, Response } from "@angular/http";
import { Observable as RxObservable } from "rxjs/Observable";

import "rxjs/add/operator/map";
import "rxjs/add/operator/do";
import "rxjs/add/operator/toPromise";
import { RestService } from './rest.service';
import { SocketService } from './socket.service';
import { Router } from '@angular/router';


@Injectable()
export class AuthService {
  handleRefreshError(arg0: any): any {
    console.log(arg0);
    this.logOut();
  }
  handleLoginErrors(arg0: any): any {
    console.log(arg0);
    return this.refreshToken();
  }

  private auth_token: string = "";
  private refresh_token: string = "";
  private expire_date: number = 0;
  private isLoggedIn: boolean = false;
  private client_id: number = 2;
  private client_secret:string =  "ZdnLE6QlqpV0ZvbiplGxKr5bEDzDnoZQGJYqszsQ";

  
  constructor(private router: Router, private routes: RoutesService, private http: Http, private socket: SocketService) { }
  get headers(): Headers {
    let head = new Headers();
    if(this.isLoggedIn){head.append('Authorization', 'Bearer ' + this.auth_token);
    head.append('Content-Type', 'application/json');}
    return head;
  }
  getToken(username: string, password: string) : Promise<Response>{
    let data = {
      password: password,
      username: username,
      grant_type: 'password',
      client_id: this.client_id,
      client_secret: this.client_secret,
     
    }
    
    let head = new Headers();
    console.log(data.password);
    return this.http.post( this.routes.getTokenRoute, data).toPromise().then(
      r=>{
        let dat =r.json(); 
        this.auth_token = dat.access_token; 
        
        this.refresh_token = dat.refresh_token; 
        this.expire_date = dat.expires_in; 

        this.logIn(); 
        return r}
    ).catch(r=>{this.handleLoginErrors(r); return r});
  }
  setSocketHeaders(){
    this.socket.setAuthToken("Bearer " + this.auth_token);
  }
  clearSocketHeaders(){
    this.socket.setAuthToken("");
  }
  refreshToken(): Promise<Response>{
    let data = {
      client_id : this.client_id,
      client_secret: this.client_secret,
      
      grant_type: 'refresh_token',
      refresh_token: this.refresh_token
    }
    return this.http.post(this.routes.getTokenRoute, data).toPromise().then(
      r=>{let dat =r.json(); this.auth_token = dat.auth_token, this.refresh_token = dat.refresh_token, this.expire_date = dat.expires_in;this.logIn(); return r}
    ).catch(r=>{this.handleRefreshError(r); return r});;
  }
  logIn(){
    this.isLoggedIn = true;
    this.socket.connect();
    this.setSocketHeaders();
    this.router.navigate(['home']);
  }
  logOut(){
    this.auth_token = "";
    this.refresh_token = "";
    this.expire_date = 0;
    this.isLoggedIn = false;
    this.clearSocketHeaders();
    this.socket.unsubscribeAll();
    this.socket.disconnect();
    this.router.navigate(['landing']);
  }
  get LoggedIn():boolean{
    return this.isLoggedIn;
  }
}
