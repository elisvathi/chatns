import { Injectable } from '@angular/core';

@Injectable()
export class RoutesService {
  private root : string = 'http://vps463181.ovh.net';
  private conversations: string = 'api/conversations'
  private messages: string = 'api/messages';
  private currentUser: string = 'api/user';
  private users: string = 'api/users'
  private getToken: string = 'oauth/token';
  private register: string = 'api/createuser';
  private refreshToken: string = 'oauth/token/refresh';
  
  constructor() { 

  }
  messagesRoute(id: number): string{
    return this.join(this.messages, id );
  }
  get conversationsRoute(): string{
    return this.join(this.conversations);
  }
 conversationsPostRoute(id:number){
   return this.join(id, this.conversations);
 }
 get currentUserRoute():string{
    return this.join(this.currentUser);
  }
  get usersRoute():string{
    return this.join(this.users);
  }
  get getTokenRoute():string{
    return this.join(this.getToken);
  }
  get refreshTokenRoute():string{
    return this.join(this.refreshToken);
  }
  get registerRoute():string{
    return this.join(this.register);
  }

  private join(...elements):string{
    let str = elements.join('/');
    return this.root + '/' + str;
  }
  get socketUrl():string{
    return this.root + ":6001";
  }
  

}
