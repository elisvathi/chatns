import { Injectable } from '@angular/core';
import { Socket } from 'nativescript-socket.io';
import { RoutesService } from './routes.service';
@Injectable()
export class SocketService {
  socket: Socket
  namespace: string = "App.Events"
  channelNames: string[];
  privateChannels: string[];
  events: string[];
  url: string;
  opts: Options;
  connected: boolean;
  constructor(private routes: RoutesService) {
    this.url = this.routes.socketUrl;
    this.channelNames = [];
    this.privateChannels = [];
    this.events = [];
    this.opts = {
      appId: "MobApp",
      key: "87f625be81533b7d1472d0d7228d08f6",
      auth: {
        headers: {
          Authorization: ""
        }
      }
    }
   

  }
  connect(){
    this.socket = new Socket(this.url);
    this.socket.connect();
    this.socket.on('reconnect', () => this.reapplyChannels());
    this.socket.on('disconnect', () => this.reconnect());
    this.connected = true;
  }
  disconnect(){
    this.unsubscribeAll();
    this.connected = false;
  }
  setAuthToken(token) {
    this.opts.auth.headers.Authorization = token;
    console.log(this.opts);
  }
  channel(name): SocketService {
    if (this.channelNames.indexOf(name) == -1) {
      this.channelNames.push(name);
    }
    this.socket.emit('subscribe', { channel: name })
    return this;
  }
  private(name): SocketService {
    if (this.privateChannels.indexOf(name) == -1) {
      this.privateChannels.push(name);
    }
    this.socket.emit('subscribe', { channel: 'private-' + name, auth: this.opts.auth, appId: this.opts.appId, key: this.opts.key })
    return this;
  }
  unsubscribe(name): SocketService {
    var removed = this.channelNames.splice(this.channelNames.indexOf(name), 1);
    this.socket.emit('unsubscribe', { channel: removed[0] });
    return this;
  }
  unsubscribePrivate(name):SocketService{
    var removed = this.privateChannels.splice(this.privateChannels.indexOf(name), 1);
    this.socket.emit('unsubscribe', {channel:'private-' + removed[0]})
    return this;
  }
  unsubscribeAll(): SocketService {
    this.channelNames.forEach(element => {
      this.unsubscribe(element)
    });
    this.privateChannels.forEach(element=>{
      this.unsubscribePrivate(element);
    })
    return this;
  }
  bind(event, callback): SocketService {
    if (this.events.indexOf(event) == -1) {
      this.listen(event)
    }
    let index = this.events.indexOf(event);
    this.socket.addEventListener(this.formatEvent(this.events[index]), callback);
    return this;
  }
  unbind(event, callback): SocketService {
    let index = this.events.indexOf(event);
    if (index != -1) {
      this.socket.removeEventListener(this.formatEvent(this.events[index]), callback);
    }
    return this;
  }

  listen(event, callback = null): SocketService {
    if (this.events.indexOf(event) != -1) {
      this.events.push(event);
    }
    var _ev = this.formatEvent(event);
    this.socket.on(_ev, (a, b) => { callback(b) });
    return this;
  }
  formatEvent(event): string {
    var data = this.namespace.split('.')
    data.push(event);
    var retVal = data.join('\\');
    return retVal;
  }
  reapplyChannels() {
    if (this.channelNames) {
      this.channelNames.forEach(element => {
        this.channel(element);
      });
    }
    if (this.privateChannels) {
      this.privateChannels.forEach(el => { this.private(el) })
    }
  }
  reconnect() {
    if(this.connected){
    this.socket.connect();}

  }
}

interface Options {
  appId: string,
  key: string
  auth: Auth
}
interface Auth {
  headers: AuthHeaders
}
interface AuthHeaders {
  Authorization: string
}
