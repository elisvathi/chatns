import { Injectable, NgZone, EventEmitter } from '@angular/core';
import { RestService } from './rest.service';
import { RoutesService } from './routes.service';
import { User, Conversation, MessageMap, Message, MessageGroup } from '../models/user';
import { SocketService } from './socket.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class DataService {
  currentUser: User;
  users: User[];
  conversations: Conversation[];
  messages: MessageMap;
  constructor(private rest: RestService,private ngZone: NgZone, private routes: RoutesService, private socket: SocketService) { }
  convUpdated: EventEmitter<number> = new EventEmitter<number>();
  fetchUser():Promise<Response>{
    return this.rest.get(this.routes.currentUserRoute).then(r=>{
      this.currentUser=r.json() as User;
      this.socket.private('App.User.' + this.currentUser.id);
       return r}).catch(r=> {return r});
  }
  fetchUsers():Promise<Response>{
    return this.rest.get(this.routes.usersRoute).then(r=>{this.users=r.json()as User[]; return r}).catch(r=>{return r});
  }
  fetchMessages(id:number):Promise<Response>{
    return this.rest.get(this.routes.messagesRoute(id)).then(r=>
      {if(this.messages == null){this.messages={}}
      this.messages[id]= new MessageGroup(r.json() as Message[]); 
      this.convUpdated.emit(id); 
      return r;}).catch(r=>{return r});
  }
  fetchConversations():Promise<Response>{
    return this.rest.get(this.routes.conversationsRoute).then(r=>
      {this.conversations = r.json()as Conversation[];
        this.subscribeToMessages(); 
        return r}).catch(r=>{return r});
  }
  subscribeToMessages(){
    this.socket.listen('NewMessage', (m)=>{
    this.ngZone.run(()=>{
      let mes: Message = m.message;
      let conv = this.getConversation(mes.conversation_id);
      let cnv = this.conversations[conv];
      cnv.lastMessage = mes;
      if(this.messages && this.messages[mes.conversation_id]){
      this.messages[mes.conversation_id].addMessage(mes);}
      this.conversations[conv] = cnv;
      this.convUpdated.emit(mes.conversation_id);
      })
    })
  }
  getConversationMessages(id):MessageGroup{
    return this.messages[id];
  }
  getConversation(id):number{
    return this.conversations.findIndex((x)=>{return x.id == id});
  }
 
  addMessage(id: number, message: Message){
    this.messages[id].addMessage(message);
    this.conversations[this.getConversation(id)].lastMessage = message;
  }
}
