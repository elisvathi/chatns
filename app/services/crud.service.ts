import { Injectable } from '@angular/core';
import { RestService } from './rest.service';
import { RoutesService } from './routes.service';
import { Message } from '../models/user';

@Injectable()
export class CrudService {

  constructor(private http: RestService, private routes: RoutesService) { }
  sendMessage(id:number,mes: string){
   return  this.http.post({message: mes}, this.routes.messagesRoute(id));
  }
  createConversation(ids: number[], text:string, convId: number){
   return this.http.post({userId: ids, messageText: text}, this.routes.conversationsPostRoute(convId));
  }
}
