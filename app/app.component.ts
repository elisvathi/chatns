import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";


@Component({
    selector: "ns-app",
    templateUrl: "app.component.html",
    styleUrls: ["app.css"],
})

export class AppComponent implements OnInit {
    ngOnInit(): void {
        this.router.navigate(["landing"]);
    }
    constructor(private router: Router) {
        
    }
 }
