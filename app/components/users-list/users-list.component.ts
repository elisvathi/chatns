import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { User } from '../../models/user';

@Component({
  moduleId: module.id,
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {
  fetched:boolean= false;
  fetching:boolean =  false;
  error: any = null;
  constructor(private data: DataService) { }
  get users():User[]{
    return this.data.users;
  }
  ngOnInit() { 
    if(this.data.users!=null){
      this.fetched = true; this.fetching=false; this.error = null;
    }else{
      this.fetching = true; this.fetched = false; this.error = null;
      this.data.fetchUsers().then(
        r=>{this.fetched = true; this.fetching= false; this.error= null}
      ).catch(
        r=>{this.fetched = false; this.fetching= false; this.error = r}
      )
    }
  }

}
