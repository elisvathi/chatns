import { Component, OnInit, Input } from '@angular/core';
import { UserMessageGroup, Message, Conversation } from '../../models/user';
import * as moment from 'moment';

@Component({
  moduleId: module.id,
  selector: 'app-data-message-group',
  templateUrl: './data-message-group.component.html',
  styleUrls: ['./data-message-group.component.scss']
})
export class DataMessageGroupComponent implements OnInit {
  @Input() date: Date;
  @Input() group: UserMessageGroup;
  @Input() conversation: Conversation;

  get formattedDate(){
    return moment(this.date).fromNow();
  }
  get groups(): Message[][] {
    let retVal: Message[][] = [];
    for (var index in this.group) {
      retVal.push(this.group[index]);
    }
    return retVal;
  }
  constructor() { }

  ngOnInit() {
   
   }

}
