import { Component, OnInit, Input } from '@angular/core';
import { User } from '../../models/user';

@Component({
  moduleId: module.id,
  selector: 'app-user-row',
  templateUrl: './user-row.component.html',
  styleUrls: ['./user-row.component.scss']
})
export class UserRowComponent implements OnInit {
@Input() user: User;
  constructor() { }
  get name(){
    return this.user.name;
  }
  ngOnInit() { }

}
