import { Component, OnInit, Input } from '@angular/core';
import { Message, User, Conversation } from '../../models/user';

@Component({
  moduleId: module.id,
  selector: 'app-message-group',
  templateUrl: './message-group.component.html',
  styleUrls: ['./message-group.component.scss']
})
export class MessageGroupComponent implements OnInit {
  @Input() messages : Message[];
  @Input() conversation: Conversation;
  
  constructor() { }

  ngOnInit() { 
    
  }

}
