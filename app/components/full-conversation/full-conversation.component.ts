import { Component, OnInit, Input, AfterViewInit, OnChanges, ViewChild, ElementRef, ViewChildren, QueryList, SimpleChanges } from '@angular/core';
import { DataService } from '../../services/data.service';
import { Conversation, Message, ConcreteConversation, MessageGroup, UserMessageGroup } from '../../models/user';
import { ActivatedRoute, Router } from '@angular/router';
import "rxjs/add/operator/switchMap"
import 'rxjs/Rx';
import { PageRoute, RouterExtensions } from 'nativescript-angular/router';
import { ListViewComponent } from 'nativescript-angular';
import { CrudService } from '../../services/crud.service';
import * as moment from 'moment';
@Component({
  moduleId: module.id,
  selector: 'app-full-conversation',
  templateUrl: './full-conversation.component.html',
  styleUrls: ['./full-conversation.component.scss']
})
export class FullConversationComponent implements OnInit {

  get conversation(): Conversation {
    return this.data.conversations[this.data.getConversation(this.id)];
  }
  @ViewChild('scrollArea') scrollArea: any;

  id: number;
  fetched: boolean;
  fetching: boolean;
  error: Response;
  message: string = "";
  constructor(private data: DataService, private rotue: PageRoute, private crud: CrudService, private router: RouterExtensions) {
    this.rotue.activatedRoute.switchMap(
      activatedRoute => activatedRoute.params)
      .forEach((params) => { this.id = +params["id"] });
      this.data.convUpdated.subscribe((x)=>{if(x==this.id){this.scrollToBottom()}})

  }
  get messages(): MessageGroup {
    let dat = this.data.getConversationMessages(this.conversation.id);
    return dat;
  }
  get isAvailable(): boolean {
    return this.conversation.available_users.findIndex((x) => { return x.id == this.data.currentUser.id }) != -1;
  }


  get dates(): Date[] {
    let retVal: Date[] = [];
    for (var dt in this.messages.results) {
      retVal.push(new Date(dt));
    }
    return retVal;
  }
  get groups(): UserMessageGroup[] {
    let retval: UserMessageGroup[] = [];
    for (var dt in this.messages.results) {
      retval.push(this.messages.results[dt]);
    }
    return retval;
  }
  get concreteConversation(): ConcreteConversation {
    return new ConcreteConversation(this.conversation);

  }
  get title() {
    return this.concreteConversation.title();
  }

  ngOnInit() {
    let a = this.data.messages;
    if (!this.data.messages || !this.data.messages.hasOwnProperty(this.conversation.id)) {
      this.fetching = true; this.fetched = false; this.error = null;
      this.data.fetchMessages(this.conversation.id).then(
        r => {
          this.fetched = true; this.fetching = false; this.error = null;
          // this.scrollToBottom();
        }
      ).catch(
        r => {
          this.fetched = false; this.fetching = false; this.error = r;
        });
    } else {

      this.fetched = true;
      this.fetching = false;
      this.error = null;
    }
    this.scrollToBottom();
  }

  scrollToBottom() {


    if (this.scrollArea) {
      var offset = this.scrollArea.nativeElement.scrollableHeight; // get the current scroll height
      this.scrollArea.nativeElement.scrollToVerticalOffset(offset+500, true);
    }
  }
  sendMessage() {
    if (this.message != "") {
      this.crud.sendMessage(this.conversation.id, this.message).then(
        r => this.message = ""
      );
    }
  }
  public onNavBtnTap() {
    this.router.back();
  }
}
