import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { registerElement } from "nativescript-angular/element-registry";

registerElement("Mapbox", () => require("nativescript-mapbox").MapboxView);

import * as geolocation from "nativescript-geolocation";
import { Accuracy } from "ui/enums"; // used to describe at what accuracy the location should be get
registerElement("MapView", () => require("nativescript-google-maps-sdk").MapView);


@Component({
  moduleId: module.id,
  selector: 'app-discover',
  templateUrl: './discover.component.html',
  styleUrls: ['./discover.component.scss']
})
export class DiscoverComponent implements OnInit {
  location: any;
  constructor() { }
  @ViewChild('map') map: ElementRef
  ngOnInit() {
   
    
    // console.log(JSON.stringify(geolocation));
    // var b= 2;
   }
  onMapReady(event){
    var geolocation = require("nativescript-geolocation");
    geolocation.enableLocationRequest();
    var mapsModule = require("nativescript-google-maps-sdk")
    geolocation.getCurrentLocation({ desiredAccuracy: Accuracy.high, updateDistance: 0.1, maximumAge: 5000, timeout: 10000 }).then(
      r=>{console.log(JSON.stringify(r));
        this.map.nativeElement.latitude = r.latitude;
        this.map.nativeElement.longitude =r.longitude;
       
        this.map.nativeElement.zoom = 15;
        var marker = new mapsModule.Marker();
        marker.position = mapsModule.Position.positionFromLatLng(r.latitude, r.longitude);
        marker.title = "YOU";
        this.map.nativeElement.addMarker(marker);
      }
    ).catch(
      r=>{console.log("ERROR LOCATION"); console.log(JSON.stringify(r))}
    );
    console.log("MAP READY");
    
    // this.map.nativeElement.latitude = this.location.latitude;
    // this.map.nativeElement.longitude = this.location.longitude; 
  }
}
