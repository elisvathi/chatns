import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';

@Component({
  moduleId: module.id,
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {

  constructor(private data: DataService) { }


  get greeting():string{
    return "Welcome " + this.data.currentUser.name;
  }
  ngOnInit() { }

}
