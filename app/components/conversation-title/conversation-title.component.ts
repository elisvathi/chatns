import { Component, OnInit, Input } from '@angular/core';
import { ConcreteConversation, Conversation, User } from '../../models/user';
@Component({
  moduleId: module.id,
  selector: 'app-conversation-title',
  templateUrl: './conversation-title.component.html',
  styleUrls: ['./conversation-title.component.scss']
})
export class ConversationTitleComponent implements OnInit {
  @Input() conversation: Conversation;
  @Input() user: User;
  constructor() { }
  get concreteConversation(): ConcreteConversation{
    return new ConcreteConversation(this.conversation);

  }
  get title(){
    return this.concreteConversation.title(this.user);
  }
  get lastMessage(){
    return this.concreteConversation.lastMessage;
  }
  get lastMessageTime(){
   let time = this.conversation.lastMessage.created_at;
   return time.toLocaleString();
   
  }
  ngOnInit() { }

}
  