import { Component, OnInit, Input } from '@angular/core';
import { DataService } from '../../services/data.service';
import { Message, Conversation, User } from '../../models/user';
import * as moment from 'moment';
@Component({
  moduleId: module.id,
  selector: 'app-single-message',
  templateUrl: './single-message.component.html',
  styleUrls: ['./single-message.component.scss']
})
export class SingleMessageComponent implements OnInit {
@Input() message: Message;
@Input() conversation: Conversation;
@Input() displayUser: boolean;
  constructor(private data: DataService) { }
  get text():string{
    return this.message.message;
  }
  get date():string{
    let time = this.message.created_at;
    // time.setMinutes(time.getMinutes()-time.getTimezoneOffset());
    return moment(time).fromNow();
  }


  get messageClass():string{
    if(this.isByCurrentUser){
      let str = "message currentUserMessage"
      if(this.displayUser){
        str+= " firstCurrent"
      }else {str+=" simpleCurrent"}
      return str;
    }else{
      let str =  "message otherUserMessage"
      if(this.displayUser){
        str+= " firstOther"
      }else {str+=" simpleOther"}
      return str;
    }
   
  }
  get user():User{
    let user =  this.conversation.users.find((x)=>{return x.id == this.message.user_id});
    if(!user)return this.data.currentUser;
    return user;
  }
  get userName(): string{
    return this.user.name;
  }
  get isByCurrentUser():boolean{
    return this.user.id == this.data.currentUser.id;
  }
  ngOnInit() { 
  
  }


}
