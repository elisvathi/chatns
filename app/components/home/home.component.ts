import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { DataService } from '../../services/data.service';

@Component({
  moduleId: module.id,
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {


  
  constructor(private data: DataService, private auth: AuthService) { }
  fetched: boolean = false;
  fetching: boolean =  false;
  error:any =  null;
  
  ngOnInit() { 
    if(this.data.currentUser){
      // this.data.fetchUser()
      this.fetched = true;
      this.fetching = false;
      this.error = null;
    }else{
      this.fetching  = true; this.fetched = false; this.error = null;
      this.data.fetchUser()
      .then(
        r=>{this.fetched = true; this.fetching = false; this.error = null})
      .catch(r=>{this.fetched = false; this.fetching = true; this.error = null})
    }
  }
  logOut(){
    this.auth.logOut();
  }
}
