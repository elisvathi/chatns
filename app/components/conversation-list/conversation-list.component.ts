import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { Conversation, User } from '../../models/user';
import { Router } from '@angular/router';

@Component({
  moduleId: module.id,
  selector: 'app-conversation-list',
  templateUrl: './conversation-list.component.html',
  styleUrls: ['./conversation-list.component.scss']
})
export class ConversationListComponent implements OnInit {
  fetched: boolean = false;
  fetching: boolean = false;
  error: any = null;
  constructor(private data: DataService, private nav: Router) { }

  get currentUser(): User {
    return this.data.currentUser;
  }
  get conversations(): Conversation[] {
    return this.data.conversations;
  }
  ngOnInit() {
    this.fetching = true;
    if (this.data.conversations != null) {
      this.fetched = true;
      this.fetching = false;
      this.error = null;
    } else {
      this.fetched = false;
      this.fetching = true;
      this.error = null;
      this.data.fetchConversations().then(
        r => { this.fetched = true; this.fetching = false; this.error = null })
        .catch(r => { this.error = r; this.fetched = false; this.fetching = false; })
    }
  }
  public onItemTap(args) {
    console.log("------------------------ ItemTapped: " + args.index);
    console.log('conversation', this.conversations[args.index].id);
    this.nav.navigate(['conversation' ,this.conversations[args.index].id ])
  }

}

