import * as moment from 'moment';
import { Moment } from 'moment';
export interface User {
    id: number;
    name: string;
    email: string;
}

export interface Message{
    id: number;
    message: string;
    conversation_id: number;
    user_id: number;
    created_at: Date;
}




export interface Conversation{
    id: number;
    lastMessage: Message;
    users: User[];
    available_users: User[];
}

export class ConcreteConversation{
    conversation: Conversation;
    constructor(conv: Conversation){
        this.conversation = conv;
    }
    title(u:User = null): string{
        if(!u || this.conversation.available_users.indexOf(u)==-1){
        let str = this.conversation.available_users[0].name.split(' ')[0];   
        for(var i = 1; i< this.conversation.available_users.length;i++){
            str+= " and " + this.conversation.available_users[i].name.split(' ')[0];
        }
        return str;}else{
            let str = "You";
            for(var i =0;i< this.conversation.available_users.length;i++){
                if(this.conversation.available_users[i]!=u){str+=" and "+ this.conversation.available_users[i].name.split(" ")[0]}
            }
            return str;
        }
        
    }
    get lastMessage():string{
        return this.conversation.lastMessage.message;
    }
    
}

export interface MessageMap{
    [conversationId: number]: MessageGroup;
}

export interface DateMessageGroup{
    [date: string]: UserMessageGroup;
}

export interface UserMessageGroup{
    [id: string]: Message[];
}

export class MessageGroup{
    lastMessage: Message;
    lastDate: Moment;
    lastIndex: number;
   results: DateMessageGroup = {};
   constructor(messages:Message[]){
        let dateIndex = moment(messages[0].created_at).startOf('day');
        let usersIndex = 0;
        let lastId = messages[0].user_id;
        let b =0;
        this.results[dateIndex.toString()] = {};
        this.results[dateIndex.toString()][usersIndex] = [];
        messages.forEach((x)=>{
       
            let date = moment(x.created_at).startOf('day');
           
        if(date.diff(dateIndex)!=0){
            usersIndex = 0;
            dateIndex = date;
            lastId = x.user_id;
            this.results[date.toString()] = {};
            this.results[date.toString()][usersIndex]= [];
        }
        if(x.user_id!=lastId){
            usersIndex++;
            lastId= x.user_id;
            this.results[date.toString()][usersIndex] = [];
        }
        let d = dateIndex.toString();
        
        this.results[d][usersIndex].push(x);
        this.lastMessage = x;
        this.lastDate = dateIndex;
        this.lastIndex = usersIndex;
        })
        
   }
   
   
   
   addMessage(message: Message){
       let date =  moment(message.created_at).startOf('day');
       if(date.diff(this.lastDate)!=0){
           this.results[date.toString()] = {}
           this.results[date.toString()][0] = [message];
            this.lastMessage = message;
            this.lastDate = date;
            this.lastIndex = 0;
       }else{
        if(this.lastMessage.user_id!=message.user_id){
            this.results[this.lastDate.toString()][++this.lastIndex] = [message]
            this.lastMessage = message;
        }else{
            this.results[this.lastDate.toString()][this.lastIndex].push(message);
            this.lastMessage = message;
        }
       }


   }
}